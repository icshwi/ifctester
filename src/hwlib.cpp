#include <cstdint>
#include <iostream>

extern "C" {
    #include <tsculib.h>
}

#include "hwlib.h"

int init_ifc14xx(int card)
{
    int ret = tsc_init(card);
    if( ret < 0 )
    {
        std::cout << "[ERROR] tsc_init returned " << ret << std::endl;
    }
    return ret;
}


void close_ifc14xx(int tscfd)
{
    ( void )tsc_exit(tscfd);
}

int ifc14xx_reg_wr(int tscfd, uint32_t addr_offset, uint32_t value, uint8_t silence )
{
    int tsc_ret = -1;
    const int regaddr = OFFSET_XUSER_TCSR + (addr_offset*4);

    tsc_ret = tsc_csr_write(tscfd, regaddr, ( int* )&value );
    if( tsc_ret < 0 )
    {
        std::cout << "[ERROR] tsc_csr_write returned " << tsc_ret << std::endl;
    }
    else
    {
        if( !silence )
        {
            ( void )fprintf(stdout, "[REG_WR] TCSR 0x%02x = 0x%08x\n", regaddr, value);
        }
    }
    return tsc_ret;
}


int ifc14xx_reg_rd(int tscfd, uint32_t addr_offset, uint32_t* value, uint8_t silence )
{
    int tsc_ret = -1;
    const int regaddr = OFFSET_XUSER_TCSR + (addr_offset*4);
    
    tsc_ret = tsc_csr_read(tscfd, regaddr, ( int* )value );
    if( tsc_ret < 0 )
    {
         std::cout << "[ERROR] tsc_csr_read returned " << tsc_ret << std::endl;
    }
    else
    {
        if( !silence )
        {
            ( void )fprintf(stdout, "[REG_RD] TCSR 0x%02x = 0x%08x\n", regaddr, *value);
        }
    }

    return tsc_ret;
}



// /* Functions for accessing 0x60 to 0x6F (SCOPE MAIN TCSR) */
// int ifc_scope_tcsr_read(int fd, int register_idx, int32_t *i32_reg_val)
// {
//     return ifc_tcsr_read(ifcdevice, OFFSET_XUSER_CSR, 0x60 + register_idx, i32_reg_val);
// }

// int ifc_scope_tcsr_write(int fd, int register_idx, int32_t value)
// {
//     return ifc_tcsr_write(ifcdevice, OFFSET_XUSER_CSR, 0x60 + register_idx, value);
// }