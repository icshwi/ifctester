#include <iostream>
#include <string>
#include <utility>
#include <iomanip>
#include <mutex>
#include <stdexcept>
#include <list>
#include <ctime>

extern "C" {
    #include "tsculib.h"
}

#include "hwlib.h"


int main() {
    std::cout << "Hello World!" << std::endl;

    /* Initialize tsc library */
    int tsc_handler = init_ifc14xx(0);

    if (tsc_handler < 0) {  
        return -1;
    }

    int val;
    ifc14xx_reg_rd(tsc_handler, 0x00, (uint32_t*) &val, 0);
    ifc14xx_reg_rd(tsc_handler, 0x60, (uint32_t*) &val, 0);
    ifc14xx_reg_rd(tsc_handler, 0x61, (uint32_t*) &val, 0);

    close_ifc14xx(tsc_handler);
    return 0;
}
