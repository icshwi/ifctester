# IFC1410

Test applications for IFC14xx hardware

## Build with CMake using Tosca Library as submodule

CMake can be used to build *ifctester* together with *tsclib* from the upstream *tsc* repository.


### Prerequisites

* CMake (version 2.8 or above)
* ppc64e6500 toolchain
* Native gcc [optional]

### Instructions

1. Update the tsc submodule project: `git submodule update --init`;
2. Configure correctly the cross-compilation settings in `cmake/ppc65e6500.cmake`;
3. Create a `build` directory inside `ifctester` top area;

```bash
cd build
cmake -DCMAKE_TOOLCHAIN_FILE=../cmake/ppc64e6500.cmake ..
make
```
The `ifctester` executable will be found inside `build/src`.

Optionally, one can configure a destination address for the binary file. Set the `CMAKE_INSTALL_PREFIX` variable in `CMakeLists.txt`. 
