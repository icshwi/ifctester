#ifndef __HWLIB_H__
#define __HWLIB_H__

#include <cstdint>

#define TCSR_ACCESS_ADJUST 0x00000000
#define OFFSET_XUSER_TCSR 0x1000

int  init_ifc14xx(int card);
void close_ifc14xx(int tscfd);
int  ifc14xx_reg_wr(int tscfd, uint32_t addr_offset, uint32_t value, uint8_t silence );
int  ifc14xx_reg_rd(int tscfd, uint32_t addr_offset, uint32_t* value, uint8_t silence );

#endif /* __HWLIB_H__ */

