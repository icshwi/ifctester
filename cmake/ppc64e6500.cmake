# Basic definitions
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_VERSION 4.14.67)
set(CMAKE_SYSTEM_PROCESSOR ppc64)

# This variable should point to ppc64e6500 SDK location
set(SDK_DIR /opt/ifc14xx/2.6-4.14)

# Location of root file system and toolchain programs
set(SDKTARGETSYSROOT ${SDK_DIR}/sysroots/ppc64e6500-fsl-linux)
set(PPC64TOOLS ${SDK_DIR}/sysroots/x86_64-fslsdk-linux/usr/bin/powerpc64-fsl-linux)

# Definition of the build tools
set(CMAKE_C_COMPILER    ${PPC64TOOLS}/powerpc64-fsl-linux-gcc)
set(CMAKE_CXX_COMPILER  ${PPC64TOOLS}/powerpc64-fsl-linux-g++)
set(CMAKE_LINKER        ${PPC64TOOLS}/powerpc64-fsl-linux-ld)
set(CMAKE_OBJCPY        ${PPC64TOOLS}/powerpc64-fsl-linux-objcpy)
set(CMAKE_RANLIB        ${PPC64TOOLS}/powerpc64-fsl-linux-ranlib)
set(CMAKE_NM            ${PPC64TOOLS}/powerpc64-fsl-linux-nm)

## BUG in CMAKE: https://gitlab.kitware.com/cmake/cmake/-/issues/15547
set(CMAKE_AR            ${PPC64TOOLS}/powerpc64-fsl-linux-ar CACHE FILEPATH "" FORCE) 

# where is the target environment
set(CMAKE_FIND_ROOT_PATH ${SDKTARGETSYSROOT})

# Compiler flags
set(CMAKE_C_FLAGS "-mhard-float -m64 -mcpu=e6500 --sysroot=${SDKTARGETSYSROOT} -O2 -pipe -g -feliminate-unused-debug-types" CACHE STRING "" FORCE)
set(CMAKE_CXX_FLAGS "${CMAKE_C_FLAGS}" CACHE STRING "" FORCE)

# Linker flags
set(CMAKE_EXE_LINKER_FLAGS_INIT "--sysroot=${SDKTARGETSYSROOT}")
set(CMAKE_SHARED_LINKER_FLAGS_INIT  "--sysroot=${SDKTARGETSYSROOT}")

# search for programs in the build host directories
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)

# for libraries and headers in the target directories
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ALWAYS)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ALWAYS)

